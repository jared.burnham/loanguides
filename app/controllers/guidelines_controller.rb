class GuidelinesController < ApplicationController

    #Index controller, get filterrific working
    def index
      #Instantiate Filterific, and respond to the forms
      @filterrific = Filterrific.new(
        Guideline, params[:filterrific]
      ) or return

      @guidelines = Guideline.filterrific_find(@filterrific)

      if params[:filterrific]
        if params[:filterrific][:is_document] == 0
          @docs = true
        else
          @docs = false
        end
      else
        @docs = true
      end

      respond_to do |format|
        format.html
        format.js
        format.json {api(params)}
      end
    end

    #Controller for the API endpoints called
    #by the underwriter's program
    #all of the ones that could be changed to scopes have been changed
    def api(params)
      @guideline = Guideline.published.note_date(params[:note_date])
      @guideline = @guideline.client(params[:client]) if params[:client]
      @guideline = @guideline.investor(params[:investor]) if params[:investor]
      @guideline = @guideline.program(params[:program]) if params[:program]
      @guideline = fullSearch(params[:search_text], params[:ids]) if params[:search_text]

      #Theses are not really used they are left in case you want to use them.
      @guideline = findChildren(params[:parent]) if params[:parent]
      @guideline = findParent(params[:child]) if params[:child]
      @guideline = findAllChildren(params[:allchilds]) if params[:allchilds]

      render json: @guideline
    end

    #Controller for the OLD search page
    def search
    end

    #Defining Guideline Creation
    def new
      @guideline = Guideline.new
    end
    def create
      @guideline = Guideline.new(guideline_params)

      @guideline.save

      redirect_to @guideline
    end

    #Edit, bring up the given Guide by ID
    def edit
      @guideline = Guideline.find(params[:id])
      @guideline.update_attribute(:recent_view_at, DateTime.current())
    end

    def big_edit
      @guideline = Guideline.find(params[:id])
      render 'big_edit'
    end

    #Show, bring up the given Guide by ID
    def show
      if params[:date] && params[:date] != ""
        @version_date = Date.parse(params[:date])
      else
        @version_date = Date.today()
      end
      @guideline = Guideline.find(params[:id])
      @guideline.update_attribute(:recent_view_at, DateTime.current())
      respond_to do |format|
        format.html
        format.json { render json: @guideline }
      end
    end

    #Delete a guide and all its descendents,
    #then redirect back to the correct page.
    def destroy

      @guideline = Guideline.find(params[:id])
      g = @guideline.parent_id  #store parent id to redirect to correct place when done with deletion
      gu = @guideline.descendents
      gu.each do |guide|
        Guideline.destroy(guide.id)
      end
      #Guideline.destroy(@guideline.id)
      if g != nil             #this will redirect to the view page of document that the deleted guide was a part of
        g = Guideline.find(g)
        if params[:source] == 'full'
          redirect_to guidelines_path + '/full/' + g.id.to_s
        else
          redirect_to :back
        end
      else #this redirects to that root page if what was deleted was a top-level document
        redirect_to guidelines_path
      end
    end

    #Update, handles normal single guide updates, and also the
    #'save to children' and 'override' funtionality is here.
    def update
      @guideline = Guideline.find(params[:id])
      if params[:commit] == 'Save To Children' #checks to see which type of save to do
        g = @guideline.descendents
        #check to see what check boxes are marked and apply the selected fields to all children

        if params[:start_check]
          g.each do |guide|
            @guideline.update(start_date: params[:guideline][:start_date])
            guide.start_date = @guideline.start_date
          end
        end
        if params[:end_check]
          g.each do |guide|
            @guideline.update(end_date: params[:guideline][:end_date])
            guide.end_date = @guideline.end_date
          end
        end
        if params[:program_check]
          g.each do |guide|
            @guideline.update(program: params[:guideline][:program])
            guide.program = @guideline.program
          end
        end
        if params[:investor_check]
          g.each do |guide|
            @guideline.update(investor: params[:guideline][:investor])
            guide.investor = @guideline.investor
          end
        end
        if params[:client_check]
          g.each do |guide|
            @guideline.update(client: params[:guideline][:client])
            guide.client = @guideline.client
          end
        end
        g.each do |guide|
          guide.save
        end
        redirect_to :back
      else
        if @guideline.update(guideline_params)
            redirect_to :back
        else
            render 'edit'
        end
      end
    end

    #Document Preview
    def preview
      @guideline = Guideline.find(params[:id])
      if params[:date] && params[:date] != ""
        @version_date = Date.parse(params[:date])
      else
        @version_date = Date.today()
      end
    end

    #Full Page Editor
    def full
      @guideline = Guideline.find(params[:id])
      if params[:date] && params[:date] != ""
        @version_date = Date.parse(params[:date])
      else
        @version_date = Date.today()
      end
    end

    #API Call Functions
    def findChildren(parent)
      #Returns all children
        @guideline = @guideline.find(parent)
        @guideline = @guideline.children
    end
    def findParent(child)
      #Returns the immediate parent
        @guideline = @guideline.where("id = ?", child)
        @guideline = @guideline.first.parent
    end
    def findAllChildren(parent)
      #returns all children and their children's children etc
        @guideline = @guideline.where("id = ?", parent)
        @guideline = @guideline.first.descendents
    end
    def fullSearch(text, ids)
      #returns all guides that have a text match in
      #title or body. Supports partial words and is
      #case insensitive.
        c = ids.split(',').map(&:to_i)
        @guideline = Guideline.search(text).where('id IN (?)', c).published
    end

    #When new docs/children are created, give them
    #some starting data, to not break the db
    def new_document
      if params[:newGuide][:title] == ""
        params[:newGuide][:title] = "New Document"
      end
      Guideline.create(position: '0',
                      start_date: '',
                      end_date: '',
                      client: '',
                      investor: '',
                      program: '',
                      parent_id: nil,
                      title: params[:newGuide][:title],
                      body: '',
                      published: false,
                      recent_view_at: DateTime.current()
                      )
      redirect_to root_path
    end

    ##Children automatically inherit data fields from
    ##their parent, but content fields are left 'blank'
    def new_child
      @parent = Guideline.find(params[:newChild][:parent])
      if @parent.children?
        @siblings = Guideline.where('guidelines.parent_id = ?', params[:newChild][:parent])
        pos = 1
        @siblings.each do |sib|
          if sib.position > pos
            pos = sib.position
          end
        end
        pos += 1
      else
        pos = 1
      end
      Guideline.create(position: pos,
                      start_date: @parent.start_date,
                      end_date: @parent.end_date,
                      client: @parent.client,
                      investor: @parent.investor,
                      program: @parent.program,
                      parent_id: params[:newChild][:parent],
                      title: 'New Child',
                      body: '',
                      published: false,
                      recent_view_at: DateTime.current()
                      )

    redirect_to :back
    end

    #Set a guide to published, redirect to the
    #appropriate page
    def publish
      @guideline = Guideline.find(params[:id])
      if (params[:act] == 'unp')
        @guideline.update_attribute(:published, false)
      else
        @guideline.update_attribute(:published, true)
      end
      redirect_to :back
    end

    #Publish the entire Document, Parent and all
    #descendents recursively.
    def publish_all
      @guideline = Guideline.find(params[:pub])

      if (params[:act] == 'unp')
        action = false
      else
        action = true
        @guideline.update_attribute(:published, action)
      end

      @guidelines = @guideline.descendents
      @guidelines = @guidelines.note_date(params[:date])
      @guidelines.each do |guide|
        guide.update_attribute(:published, action)
      end
      redirect_to :back
    end


    #Set standard options for the serializer
    def default_serializer_options
        {root: false}
    end

    #Serialize drag & drop list
    def serialize
      if(params[:message])
        newOrder = JSON.parse(params[:message])
        #this finds the correct document to put all of the guides under
        p = Guideline.find(newOrder[0]["id"])
        while Guideline.find(p).parent_id != nil
          p = Guideline.find(p).parent_id
        end
        reorder(newOrder, p)
        redirect_to :back
      else
        redirect_to root_path
      end
    end

    def reorder(list, parent)
      if list.respond_to?(:each)
        count = 1
        list.each do |l|
          g = Guideline.find(l["id"])
          g.position = count
          g.parent_id = parent
          g.save
          if (l["children"] != nil)
            reorder(l["children"], l["id"])
          end
          count = count + 1
        end
      else
        g = Guideline.find(list)
        g.position = count
        g.parent_id = parent
        g.save
      end
    end

#Set of standard Guideline parameters
private
    def guideline_params
        params.require(:guideline).permit(
        :id,
        :position,
        :start_date,
        :end_date,
        :client,
        :investor,
        :program,
        :parent_id,
        :title,
        :body)
    end
end
