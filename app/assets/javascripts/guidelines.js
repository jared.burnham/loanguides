$(document).ready(function() {

  $(".date").datepicker({
    dateFormat: "yy-mm-dd"
  });

  $("#toggle").click(function(){
    $("#slide").slideToggle("slow");

    if ($.trim($(this).text()) === 'Minimize') {
      $(this).text('Expand');
    } else {
      $(this).text('Minimize');
    }
  });

  $(".slider").click(function(){
    $("#exp-coll").slideToggle("slow");

    if ($("#exp-coll").is(':visible')) {
      $(this).addClass('glyphicon-plus').removeClass('glyphicon-minus');
    } else {
      $(this).addClass('glyphicon-minus').removeClass('glyphicon-plus');
    }

  });


  $('.save').on('click', function(e) {

    $(this).addClass('btn-success').removeClass('btn-primary');
    $('.saved-message').css("display", "inline");

  });

  $('.back-to-top').on('click', function(e) {
    //$(window).scrollTop(0);
    window.scrollTo(0, 0);
  });

  $('.save').tooltip();

  //live preview functions

  //render markdown on page load if there is an editor
  if($('#editor').length > 0){
    $('#previewField').html(markdown.toHTML($('#editor').val(), 'Maruku'));
    //ensure modal preview has content
    if($('#modalPreview').length > 0){
      $('#modalPreview').html(markdown.toHTML($('#editor').val(), 'Maruku'));
    }
  }
  //update preview when typing.
  //this is not tested with larger text bodies, not sure if will scale well
  $('#editor').keyup(function(){
    var newhtml = markdown.toHTML($('#editor').val(), 'Maruku')
    $('#previewField').html(newhtml);
    if($('#modalEditor').length > 0){
      $('#modalEditor').val($('#editor').val());
      $('#modalPreview').html(newhtml);
    }
  });
  $("#modalEditor").keyup(function(){
    $('#editor').val($('#modalEditor').val());
    var newhtml = markdown.toHTML($('#modalEditor').val(),'Maruku');
    $('#modalPreview').html(newhtml);
    $('#previewField').html(newhtml);
  });

  //renders the full doc preview one section at a time.
  $(".body-text").each(function(){
    $(this).html(markdown.toHTML($(this).text(), 'Maruku'));
  });

  //links scroll bars of modal editor and preview
  //this is a little laggy but shouldnt be that bad
  //also this doesn't go by % so if one is larger than
  //the other the smaller will hit the bottom before the other.
  $('.linked').scroll(function(){
    $('.linked').scrollTop($(this).scrollTop());
  });

  $('.accordion-panel').click(function(){

    var symbol = $(this).find('.accordion-symbol');

    if($(this).hasClass("collapsed")){
      symbol.removeClass('glyphicon-chevron-right');
      symbol.addClass('glyphicon-chevron-down');
    }
    else
    {
      symbol.removeClass('glyphicon-chevron-down');
      symbol.addClass('glyphicon-chevron-right');
    }

  });
});

$(window).scroll(function(){
    $(".back-to-top").css("bottom", (Math.min( 5, ($(this).scrollTop() - 300)/20).toString().concat('%')));
});

function deleteItem(guideline) {
  //document.getElementById("delete-item-name").textContent(guideline);
  $('#deleteButton').click();
  //alert($('delete-item-name').text());
}
