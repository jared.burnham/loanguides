$(document).ready(function()
{

    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        }
    };

    // activate Nestable
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $('.dd-save').click(function(){
      var message = $("#nestable-output").val();
      var url = $('#my_link').attr('href') + '?message=' + encodeURIComponent(message);
      $('#my_link').attr('href', url);
    });
});

function drag()
  {
    var panels = $('.dd-panel');
    var buttonTexts = $('.dragToggleText');
    var locks = $('.lock');
    var buttons = $('.dd-btn');

    if(locks.is(":visible"))
    {
      $('.lock').hide();
      $('.unlock').show();
      //buttonTexts.text('Save');
      panels.addClass('dd-handle');
      panels.removeClass('dd-nodrag');
      buttons.prop('disabled', true);
      buttons.addClass('disabled');
    }
    else
    {
      $('.lock').show();
      $('.unlock').hide();
      //buttonTexts.text('Reorder');
      panels.removeClass('dd-handle');
      panels.addClass('dd-nodrag');
      buttons.prop('disabled', false);
      buttons.removeClass('disabled');
    }
  }
