class Guideline < ActiveRecord::Base
    include PgSearch
    include Filterrific

    #Defining Parent/Child Relationships
    has_many :children, class_name: "Guideline", foreign_key: "parent_id", primary_key: "id"
    belongs_to :parent, class_name: "Guideline", primary_key: "id", foreign_key: "parent_id"


    #API scopes
    #This are the deinfed scopes as per code-review
    scope :published, -> { where(published: true) }
    scope :note_date, -> (date) {
      where("? >= start_date
      AND (end_date IS NULL
      OR end_date > ?)", date, date)}
    scope :investor, -> (investor) {where(investor: investor)}
    scope :program, -> (program) {where(program: program)}
    scope :client, -> (client){where(client: client)}




    #PGSearch Scopes
    multisearchable :against => [:investor]

    pg_search_scope :search, against: [:investor, :body, :title],
                                using: {tsearch: {dictionary: "english"}}
    pg_search_scope :titlesearch, against: [:title],
                                using: {tsearch: {:prefix => true}}
    pg_search_scope :bodysearch, against: [:body],
                                using: {tsearch: {:prefix => true}}

    #Create Filter Instance with prototypes of options
    filterrific :default_settings => {:is_document => 0,
                                    :pub_status => 0,
                                    :sorted_by => 'recent_desc',
                                    :with_date => Date.today()},
                :filter_names => %w[
                  with_date
                  with_investor
                  with_program
                  with_client
                  with_title
                  with_body
                  is_document
                  pub_status
                  sorted_by
                ]


  #Filteriffic Scopes
  scope :with_title, lambda { |query|
    #Filter by Title
    terms = query.downcase.split(/\s+/)

    titlesearch(terms)
  }
  scope :with_body, lambda { |query|
    #Filter by Body content
    terms = query.downcase.split(/\s+/)

    bodysearch(terms)
  }
  scope :with_investor, lambda { |investor|
    #Filters Guide list by investors

    investor = "%" + investor.downcase + "%"

    where('LOWER(guidelines.investor) LIKE ?', investor)
  }
  scope :with_client, lambda { |client|
    #Filters Guide list by client

    client = "%" + client.downcase + "%"

    where('LOWER(guidelines.client) LIKE ?', client)
  }
  scope :with_program, lambda { |program|
    #Filters Guide list by program

    program = "%" + program.to_s.downcase + "%"

    where('LOWER(guidelines.program) LIKE ?', program)
  }
  scope :with_date, lambda { |date|
    #Filter by date range

    where('guidelines.start_date IS NULL OR (guidelines.start_date <= ?
      AND (guidelines.end_date IS NULL
      OR guidelines.end_date >= ?))', date, date)

  }
  scope :is_document, lambda { |bool|
    #Unchecked: false, show only document titles
    #Checked:   true, show all guideline sections
=begin
    if bool == 0
      where('guidelines.parent_id IS NULL')
    end
=end
  }
  scope :pub_status, lambda { |pub_option|
    #Filter out based on the published status options

    if pub_option == 1
      where('guidelines.published IS TRUE')
    elsif pub_option == 2
      where('guidelines.published IS NOT TRUE')
    end
  }
  scope :sorted_by, lambda { |sort_option|
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/
      order("guidelines.created_at #{ direction }")
    when /^title_/
      order("LOWER(guidelines.title) #{ direction }")
    when /^recent_/
      order("guidelines.recent_view_at #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }

  #Ways for the Controller to access the relationships
  #Used in the API Calls
  def children?
    #IF there are children
    children.any?
  end
  def descendents
    #The call used to get all children
    #and the children of their children etc.
    self.class.tree_for(self)
  end
  def self.tree_for(instance)
    #helper for #descendents
    where("guidelines.id IN (#{Guideline.tree_sql_for(instance)})").order("guidelines.id")
  end
  def self.tree_sql_for(instance)
    #helper for #descendents
    tree_sql =  <<-SQL
      WITH RECURSIVE search_tree(id, path) AS (
          SELECT id, ARRAY[id]
          FROM guidelines
          WHERE id = #{instance.id}
        UNION ALL
          SELECT guidelines.id, path || guidelines.id
          FROM search_tree
          JOIN guidelines ON guidelines.parent_id = search_tree.id
          WHERE NOT guidelines.id = ANY(path)
      )
      SELECT id FROM search_tree ORDER BY path
    SQL
  end


  #Options group for Filterrific 'Order By' Selector
  def self.options_for_sorted_by
    [
      ['Recently Viewed', 'recent_desc'],
      ['Title (A-Z)', 'title_asc'],
      ['Title (Z-A)', 'title_desc'],
      ['Newest First', 'created_at_desc'],
      ['Oldest First', 'created_at_asc']
    ]
  end

  #Options group for Filterrific 'Status' Selector
  def self.options_for_pub_status
    [
      ['All Documents', 0],
      ['Only Published', 1],
      ['Only Unpublished', 2]
    ]
  end
end
