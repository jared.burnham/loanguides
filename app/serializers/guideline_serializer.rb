class GuidelineSerializer < ActiveModel::Serializer
  attributes :id, :title

  def url
      guideline_url(object)
  end
end
