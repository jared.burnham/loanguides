require 'rails_helper'
require 'Date'

RSpec.describe GuidelinesController, :type => :controller do
    before :each do
      Guideline.create(
          id: '1',
          position: '0',
          start_date: Date.strptime('10/30/2014', "%m/%d/%Y"),
          end_date: '',
          client: '',
          investor: 'investor',
          program: 'program1',
          parent_id: nil,
          title: 'title',
          body: 'body'
      )
      Guideline.create(
          id: '2',
          position: '1',
          start_date: Date.strptime('10/30/2014', "%m/%d/%Y"),
          end_date: '',
          client: '',
          investor: 'investor',
          program: 'program',
          parent_id: '1',
          title: 'title2',
          body: 'body'
      )
      Guideline.create(
          id: '3',
          position: '2',
          start_date: Date.strptime('10/30/2014', "%m/%d/%Y"),
          end_date: '',
          client: '',
          investor: 'investor',
          program: 'program',
          parent_id: '2',
          title: 'title3',
          body: 'body'
      )
    end
    describe '#create' do
        it "creates a guide" do
            g = Guideline.where("title = 'title'").first
            expect(g.position).to eql 0
        end
    end
    describe '#API' do
        it "finds all programs " do
            @response = JSON.parse(response.body)
            get :index, :program => 'program1'
            expect(@response.id).to eql '1'
            expect(@response.title).to eql 'title'
        end
    end
    describe '#findParent' do
        before :each do
            @guideline = controller.findParent(2)
        end
        it "finds the parent of a guideline" do
            expect(@guideline.id).to eql 1
        end
    end
    describe '#findAllChildren' do
        before :each do
          @guideline = controller.findAllChildren(1)
        end
        it "should find children down the tree" do
            expect(@guideline[0].id).to eql 1
            expect(@guideline[1].id).to eql 2
            expect(@guideline[2].id).to eql 3
        end
    end
    describe '#new_document' do
        before :each do
            post 'new_document', :newDoc => {:title => 'newDoc'}
            @guideline = Guideline.where('title = ?', 'newDoc').first
        end
        it "should create a doc with title 'newDoc'" do
            expect(@guideline.title).to eql 'newDoc'
        end
    end
end
