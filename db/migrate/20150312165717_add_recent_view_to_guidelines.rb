class AddRecentViewToGuidelines < ActiveRecord::Migration
  def change
    add_column :guidelines, :recent_view_at, :datetime
  end
end
