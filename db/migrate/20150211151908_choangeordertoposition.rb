class Choangeordertoposition < ActiveRecord::Migration
  def change
    rename_column :guidelines, :order, :position
  end
end
