class ChangeSectionIdtoOrder < ActiveRecord::Migration
  def change
    rename_column :guidelines, :section_id, :order
    #rename_column :guidelines, :parent_id, :parent
    rename_column :guidelines, :section_text, :body
    rename_column :guidelines, :section_heading, :title
  end
end
