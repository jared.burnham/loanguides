class AddParentReference < ActiveRecord::Migration
  def change
    remove_column :guidelines, :parent_id, :integer
    add_reference :guidelines, :parent
  end
end
