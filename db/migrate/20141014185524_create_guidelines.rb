class CreateGuidelines < ActiveRecord::Migration
  def change
    create_table :guidelines do |t|
      t.integer :Section_id
      t.date :start_date
      t.date :end_date
      t.text :client
      t.text :investor
      t.text :program
      t.integer :parent_id      #parent
      t.text :section_heading   #title
      t.text :section_text      #body

      t.timestamps
    end
  end
end
