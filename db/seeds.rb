# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
require 'Date'

Guideline.create(position: nil,
                    start_date: nil,
                    end_date: '',
                    client: '',
                    investor: 'Fannie Mae',
                    program: '',
                    parent_id: nil,
                    title: 'Fannie Mae Selling Guide',
                    body: '')
Guideline.create(position: '1',
                    start_date: Date.strptime('01/27/2015', "%m/%d/%Y"),
                    end_date: '',
                    client: '',
                    investor: 'Fannie Mae',
                    program: '',
                    parent_id: '1',
                    title: 'Fannie Mae Selling Guide',
                    body: '',)
Guideline.create(position: '1',
                    start_date: Date.strptime('01/27/2015', "%m/%d/%Y"),
                    end_date: '',
                    client: '',
                    investor: 'Fannie Mae',
                    program: '',
                    parent_id: '2',
                    title: 'Part A: Doing Business with Fannie Mae',
                    body: 'This part describes the requirements a lender must satisfy to become a Fannie Mae-approved seller and servicer of residential home mortgage loans. This part also includes information on an approved lender’s contractual obligations, procedures for obtaining technology applications, and requirements for maintaining lender eligibility.')
Guideline.create(position: '1',
                    start_date: Date.strptime('01/27/2015', "%m/%d/%Y"),
                    end_date: '',
                    client: '',
                    investor: 'Fannie Mae',
                    program: '',
                    parent_id: '3',
                    title: 'A1: Approval Qualification',
                    body: 'This subpart contains the requirements for becoming an approved Fannie Mae lender, an overview of the lender approval process, and a list of loan types that require special lender approval.')
Guideline.create(position: '1',
                    start_date: Date.strptime('01/27/2015', "%m/%d/%Y"),
                    end_date: '',
                    client: '',
                    investor: 'Fannie Mae',
                    program: '',
                    parent_id: '4',
                    title: 'A1-1: Application and Approval of Lender',
                    body: 'This chapter includes information on the eligibility and application requirements for lenders seeking Fannie Mae approval. It also describes loan types that require special lender approval.')
Guideline.create(position: '1',
                    start_date: Date.strptime('04/09/2013', "%m/%d/%Y"),
                    end_date: '',
                    client: '',
                    investor: 'Fannie Mae',
                    program: '',
                    parent_id: '5',
                    title: 'A1-1-01: Application and Approval of Lender ',
                    body: "
*General Information*

Lenders must be approved to do business with Fannie Mae. Fannie Mae determines a lender’s qualifications by reviewing the lender’s financial condition, organization, staffing, servicing experience, and other relevant factors.

Fannie Mae’s standard approval is for the sale and/or servicing of single-family loans (excluding those loans delivered under a negotiated contract). Lenders must obtain special approval to sell and service certain mortgages with unique requirements, such as loans secured by co-op shares or HomeStyle® renovation mortgages.

Eligibility

Approval or rejection of a lender’s application is at Fannie Mae’s sole discretion and is based on Fannie Mae’s business judgment with respect to the totality of the lender’s circumstances. At a minimum, to be considered for approval to sell and service residential first mortgages, a lender must:

have as its principal business purpose, the origination, selling, and/or servicing of residential mortgages;
have demonstrated the ability to originate, sell, and/or service the types of mortgages for which approval is being requested;
have adequate facilities and staff experienced in originating, selling, and/or servicing the types of mortgages for which approval is being requested;
be duly organized, validly existing, properly licensed (in good standing) or otherwise authorized to conduct its business in each of the jurisdictions in which it originates, sells, and services residential mortgages;
have a net worth of at least $2.5 million, plus a dollar amount that represents 0.25% of the unpaid principal balance of any Fannie Mae portfolio it is servicing. A lender’s Fannie Mae portfolio includes mortgages or participation interests in MBS pools, first and second whole mortgages held in Fannie Mae’s portfolio, Fannie Mae’s participation interests in first or second mortgages in participation pools held in its portfolio, and multifamily mortgages. Lender net worth, as defined and calculated by Fannie Mae, is the lender’s Total Equity Capital as determined by Generally Accepted Accounting Principles (GAAP), less goodwill and other intangible assets (excluding mortgage servicing rights) and, based on Fannie Mae’s assessment of associated risks, a possible deduction of “affiliate receivables” and “pledged assets net of associated liabilities” (hereinafter referred to as Lender Adjusted Net Worth). Based on specific circumstances, a lender may be required to satisfy other financial standards or additional net worth and liquidity eligibility criteria. See A4-2-01, Net Worth and Liquidity Requirements for additional information on Fannie Mae’s net worth requirements for approved lenders;
have internal audit and management control systems to evaluate and monitor the overall quality of its loan production and servicing;
have written procedures for the approval and management of vendors and other third-party service providers;
have a fidelity bond and an errors and omissions policy in effect and agree to modify them as necessary to meet Fannie Mae requirements;
satisfy any additional eligibility criteria Fannie Mae imposes. Such additional criteria may apply either to individual lenders, all lenders that are seeking approval to sell and/or service certain types of mortgages, all lenders that share certain characteristics, or all lenders. Fannie Mae approves or disapproves a lender based on an assessment of its total circumstances; therefore, a lender that satisfies Fannie Mae’s general eligibility criteria or any special criteria does not have an absolute right to be approved and should not expect automatic approval.
Lenders are not required to purchase or own Fannie Mae stock as a condition of eligibility.

Application Requirements

Lenders applying to do business with Fannie Mae must submit the documentation described on Fannie Mae's website.

Application Review Fee

The basic application review fee for new lenders is $5,000. Application review fees are not refundable.

Special Lender Approval and MSSC Addendum

Certain mortgage loan types require special approval. The following special approvals will be documented by an addendum to the Mortgage Selling and Servicing Contract (MSSC) between Fannie Mae and the lender:

co-op share loans,
second mortgages,
HomeStyle renovation mortgages,
Texas Section 50(a)(6) mortgages, and
electronic mortgages (eMortgages).
Lenders may request approval to deliver these loans through their lead Fannie Mae regional office (see E-1-03, List of Contacts). Lenders may not deliver these loan types unless they obtain the applicable special lender approval and execute any additional agreements required by Fannie Mae. Lenders that apply for special approval to deliver HomeStyle renovation mortgages must also complete a Special Lender Approval Form (Form 1000A).

Fannie Mae reserves the right to cease approving lenders for or accepting deliveries of any or all of the mortgage loan types listed above from any or all lenders. The decision to no longer accept deliveries may result in an amendment to, or the termination of, the special approval. Fannie Mae will provide the affected lender(s) with reasonable notice of this decision. If the decision affects a lender's ability to fulfill any required mandatory delivery amount under its Master Agreement, Fannie Mae will consider alternatives through which the lender can fulfill its delivery obligation.

For a discussion of mortgage loan types that require special customized/negotiated terms in a Master Agreement, see A2-4-01, Master Agreement Overview. For additional information on lender contracts, refer to E-1-04, List of Lender Contracts."
)

Guideline.create(position: nil,
                    start_date: nil,
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: nil,
                    title: 'How to use this System',
                    body: 'Click the First blue link in the Tree view on the left half of the screen that says "How to use this System"')
Guideline.create(position: '1',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '7',
                    title: 'How to use this System',
                    body: 'Welcome!

This document will show you step by step how you can use all the tools that this system provides so that you can more efficiently create and update your loan guide database.

The different sections found on the left will give you in depth details on how the different aspects of this system work.

Feel free at any point to navigate away from this page and test the system yourself.')
Guideline.create(position: '1',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '8',
                    title: 'I. Documents',
                    body: "The system divides guides that Adfitech receives into separate Documents.

Managing Documents is the reason the whole system exists, so understanding how the documents are supposed to be structured and knowing all the information that the system keeps track of for the documents is important for using the system to its fullest potential.

This is the Edit Document page, and from here you can do everything you would need to do to create or update documents.  Take a look at the different sub-sections (aka children) of the 'I. Document' section on the left for more information about the specifics on Documents.")
Guideline.create(position: '2',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '8',
                    title: 'II. The Index',
                    body: "The Index is the Home Screen of the System. It lists all Guidelines, includes a filter to narrow what is in the list, and a form to Add New Documents.

The Filter includes all the Data fields that a guide section would have, along with a selector for how the list should be ordered, a selector for the Published status of the sections, and a checkbox for showing All Guide Sections. Any combinations of filters can be used at once, and the filter can easily be returned to the default settings by clicking 'Reset Filter'.

The 'All Guide Sections' checkbox will change whether Shells or All Sections will appear in the list. The default Unchecked setting will only list Document Shells.  For example, if this Shell were the only item in the database, having the box Unchecked would only list the Shell in the Index.  If the box were checked, the Shell and all of the sections would be listed, and the Sections would also list what Shell they are a part of next to their title.
")
Guideline.create(position: '3',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '8',
                    title: 'III. Markdown',
                    body: "So that you can easily format documents, a plain text to rich text interpreter has been implemented called **Markdown**.

If you scroll down a bit, the Preview section of the page will display what the rendered Markdown looks like. For example, the word Markdown is bolded because of the double asterisks surrounding it.  To see what the entire document looks like (what the Underwriters will see), click the 'Preview' button in the bottom right corner of the Tree.

Markdown is easy to use, and once someone has the basics down they can typically type it faster than they could format a document with normal editor buttons.

For more examples of how to use Markdown, check section '3.1: Examples'

")
Guideline.create(position: '1',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '9',
                    title: '1.1: The Tree',
                    body: "The left half of the Edit Document page is the Tree.

The Tree shows the structure of the Document and the titles of all the sections and sub-sections within the Document.  This system refers to sub-sections as 'Children'.  The 'I. Documents' section for example is a child of the 'Click Me First! section, and also has 4 children itself, so the 'I. Documents' section would be called the 'Parent' of this section '1.1: The Tree'.  To add a new child to the document, click the New Child button that is attached to the intended parent of the new section. New children will automatically inherit all Data from their parent, but not the Content.

If for some reason the sections are not in the intended order, simply click the 'Reorder' button at the top of the tree.  After clicking, the sections can be easily moved around in a drag-and-drop manner.  Once in the correct positions, click the 'Save' button attached to the 'Reorder' button at the top of the Tree, and their new order will be saved to the database.")
Guideline.create(position: '2',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '9',
                    title: '1.2: Editing Form',
                    body: "The right side of the Edit Document page is the Editing Form.

The Editing Form is split into two panels, Data and Content.  The Content panel holds the information found IN the Document, while the Data tab holds information ABOUT the Document.

If information in either tab is changed, the blue 'Save' button below the Editing Form needs to be clicked before navigating to another page or to another section of the same document.

If a section needs to be deleted, the red 'Delete' button at the bottom of the Edit Form will delete the section currently being viewed, and all of that sections children.  So if the 'III. Markdown' section were deleted, '3.1: Examples' would be deleted as well.  The 'Publish' and 'Unpublish' buttons will be covered in '1.3: Publishing'.

The Data panel includes multiple fields with checkboxes attached to them.  Checking the boxes and clicking the 'Save To Children' button will save the information in the checked fields to all children of the section (and children of the children and so on).

The 'Big Editor' button will open up a full screen view of the editor with a text field on the left, and a live preview on the right.  This is a useful tool for long sections, where the normal text area might not be big enough.")
Guideline.create(position: '3',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '9',
                    title: '1.3: Publishing',
                    body: "Documents and Sections can be in two different states: Published and Unpublished.  The state they are currently in is indicated in a few locations, in the tree on the left of each cell, in the Index on the left of each title, and in the Data panel.

A yellow exclamation mark means that the section is Unpublished.
A green check mark means that the section is Published.

The purpose of the two states is to show which documents are visible to the Underwriters.  A Document that is Published is complete and correct.  An Unpublished Document is one that is still being worked on, or is not correct.

If a new Unpublished Document that has been entered to the system over the course of a few days has been completed, the green 'Publish All' button on the bottom of the Tree will Publish all sections of the Document.  The 'Unpublish All' document does the exact opposite, Unpublishing all  the sections of the Document.  To Publish or Unpublish a specific section of the Document, the 'Publish' and 'Unpublish' buttons at the bottom of the Editing Form can be used.")
Guideline.create(position: '4',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '9',
                    title: '1.4: Versions',
                    body: "Due to the fact that many of the Documents being added to the system are just updates to older Documents, Versions have been implemented.

Versions of Documents are determined by the Start Date and End Date data fields.  In the top right corner of the Edit Document Page, a 'Version Date' field shows the Version that is valid for the given Date.  If you change the date to be before 2015, you will see an older version of this Document.

'Publish All' and 'Unpublish All' will only change the status of sections that are valid for the Version Date.")
Guideline.create(position: '5',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '9',
                    title: '1.5: Shells',
                    body: "Because the way that Versioning works, the System holds Documents in objects called Shells.  The 'How to use this System' section that is at the top of the tree (not inside it) is considered a Shell.

Shells are named after the Title of the Document that they hold, and cannot be given Body Content, Start Dates, or End Dates.  This is so that they will never be out of Date.  If a Shell were to be given an End Date, and held a version of the Document that was valid through to a later End Date, the Underwriter UI would not be able to access the current version of the Document.

This Shell actually has content in the Body, but that is just to help you navigate to the rest of this guide, no other Shell should ever have Body content.

Deleting a Shell will delete all versions of the Document stored inside it.

The Publish All button will Publish an unpublished Shell, but the Unpublish All button will not unpublish the Shell.  This is so certain versions of the Document can be unpublished without removing all versions from the system.  The Shell can be unpublished by selecting it and Unpublishing it in the Editing Form.")
Guideline.create(position: '1',
                    start_date: Date.strptime('01/01/2015', "%m/%d/%Y"),
                    end_date: nil,
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '11',
                    title: '3.1: Examples',
                    body: 'A single carrige return
will not create a new line

It takes two.

Numbered Lists with other formating:

1. *Italics*

1. **Bold**

Bulletted Lists work too:

* Object 1
* Object 2

>A block quote could go here.

[Links look like this](http://www.adfitech.com/ "You can even add hover info!")

Images look like this:

![Images](https://www.adfitech.com/assets/Adfitech-Logo-Blue-Horizontal-ef57fcfa0ebd16494d8b54975ae3d8a0.png)

Tables can also be used like this:

| Loan Length | Loan Value |Loan Holder|
|-----------|------------:|:-----------:|
|Less than 4 years  | $4,000| Bobby |
|Greater than 4 years  | $16,000| Diego |

Columns are automatically Left-Justified, but alignment can be changed using Colons in the Divider row like in the table above.')
Guideline.create(position: '1',
                    start_date: Date.strptime('01/01/1990', "%m/%d/%Y"),
                    end_date: Date.strptime('12/31/2014', "%m/%d/%Y"),
                    client: '',
                    investor: '',
                    program: '',
                    parent_id: '7',
                    title: 'How to Use this system',
                    body: 'This is an Old Version of the document.

Go back to 2015 to continue the tutorial.')
