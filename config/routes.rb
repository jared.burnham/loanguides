Rails.application.routes.draw do

    resources :guidelines
    get '/search' => 'guidelines#search'
    root 'guidelines#index'
    post '/new_doc' => 'guidelines#new_document'
    post '/new_child' => 'guidelines#new_child'
    post '/publish' => 'guidelines#publish'
    post '/pub_all' => 'guidelines#publish_all'
    get '/api' => 'guidelines#api'
    get 'guidelines/preview/:id' => 'guidelines#preview'
    get 'guidelines/full/:id' => 'guidelines#full'
    post '/serialize' => 'guidelines#serialize'
end
